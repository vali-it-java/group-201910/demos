package week2.bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AccountService {

    private static List<Account> customers = new ArrayList<>();

    public static void loadAccounts(String accountFilePath) throws IOException {
        List<String> allLinesFromFile = Files.readAllLines(Paths.get(accountFilePath));

        for (int i = 0; i < allLinesFromFile.size(); i++) {
            String oneLineFromFile = allLinesFromFile.get(i); //"Jean, Waters, 758818991, 2110"
            customers.add(new Account(oneLineFromFile));
        }
    }

    public static Account search(String accountNumberToSearch) { // "758818991"
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getAccountNumber().equals(accountNumberToSearch)) {
                return customers.get(i);
            }
        }
        return null;
    }

    public static Account search(String customerFirstName, String customerLastName) {
        for (Account customerAccount : customers) {
            if (customerFirstName.equals(customerAccount.getFirstName()) &&
                    customerLastName.equals(customerAccount.getLastName())) {
                return customerAccount;
            }
        }
        return null;
    }

    public static TransferResult transfer(String fromAccountNumber, String toAccountNumber, double sum) {
        // "937416401" "318129395" 35.0
        Account fromAccount = search(fromAccountNumber);
        Account toAccount = search(toAccountNumber);
        if(fromAccount != null && toAccount != null) {
            if (fromAccount.getBalance() >= sum) {
                toAccount.setBalance(toAccount.getBalance() + sum);
                fromAccount.setBalance(fromAccount.getBalance() - sum);
                return new TransferResult("OK", fromAccount, toAccount);
            } else {
                return new TransferResult(
                        "Kontol pole piisavalt vahendeid!", fromAccount, toAccount);
            }
        } else {
            return new TransferResult("Ei leitud kõiki kontosid", fromAccount, toAccount);
        }
    }
}
