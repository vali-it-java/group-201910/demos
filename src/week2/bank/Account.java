package week2.bank;

public class Account {
    private String firstName;
    private String lastName;
    private String accountNumber;
    private double balance;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Account(String accountData) {
        // "Jean, Waters, 758818991, 2110"
        String[] parts = accountData.split(", ");
        this.firstName = parts[0]; // Jean
        this.lastName = parts[1]; // Waters
        this.accountNumber = parts[2]; // 758818991
        this.balance = Double.parseDouble(parts[3]); // 2110.0
    }
}
