package week2.bank;

import java.io.IOException;
import java.util.Scanner;

public class BankApp {
    public static void main(String[] args) throws IOException {
        AccountService.loadAccounts(args[0]);
        Scanner scanner = new Scanner(System.in);

        System.out.println("SUPERPANK!");
        while (true) {
            System.out.println("Sisesta käsklus:");
            String command = scanner.nextLine();
            String[] commandParts = command.split(" ");

            switch (commandParts[0]) {
                case "EXIT": // EXIT
                    System.out.println("Head aega!");
                    return;
                case "BALANCE":
                    if(commandParts.length == 2) {
                        // BALANCE 656129320
                        Account x = AccountService.search(commandParts[1]);
                        printAccountInfo(x);
                    } else if (commandParts.length == 3) {
                        // BALANCE Jean Waters
                        Account y = AccountService.search(commandParts[1], commandParts[2]);
                        printAccountInfo(y);
                    } else {
                        System.out.println("VIGA: ebakorrektne käsklus!");
                    }
                    break;
                case "TRANSFER": // TRANSFER 656129320 780141394 1
                    TransferResult result = AccountService.transfer(
                            commandParts[1], commandParts[2],
                            Double.parseDouble(commandParts[3]));
                    if (result.getResultMessage().equals("OK")) {
                        System.out.println("Ülekanne õnnestus!");
                        System.out.println("Maksja konto:");
                        printAccountInfo(result.getFromAccount());
                        System.out.println("Saaja konto:");
                        printAccountInfo(result.getToAccount());
                    } else {
                        System.out.println("Ülekanne ebaõnnestus!");
                        System.out.println("Põhjus: ");
                        System.out.println(result.getResultMessage());
                        System.out.println("-----------------------");
                    }
                    break;
                default:
                    System.out.println("VIGA: Tundmatu käsklus!");
            }
        }
    }

    private static void printAccountInfo(Account x) {
        if (x != null) {
            System.out.println("-----------------------");
            System.out.println("Leiti konto:");
            System.out.println("Nimi: " + x.getFirstName() + " " + x.getLastName());
            System.out.println("Konto number: " + x.getAccountNumber());
            System.out.println("Konto jääk: " + x.getBalance());
            System.out.println("-----------------------");
        } else {
            System.out.println("-----------------------");
            System.out.println("Kontot ei leitud!");
            System.out.println("-----------------------");
        }
    }
}
