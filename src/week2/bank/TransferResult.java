package week2.bank;

public class TransferResult {
    private String resultMessage;
    private Account fromAccount;
    private Account toAccount;

    public String getResultMessage() {
        return resultMessage;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public TransferResult(String resultMessage, Account fromAccount, Account toAccount) {
        this.resultMessage = resultMessage;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
    }
}
