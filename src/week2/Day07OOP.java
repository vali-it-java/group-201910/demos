package week2;

import java.util.Arrays;

public class Day07OOP {
    public static void main(String[] args) {
        Athlete skydiver1 = new Skydiver("Rein", "Tamm", 89, "M", 177, 67);
        Athlete skydiver2 = new Skydiver("Mai", "Laud", 29, "N", 171, 81);
        Athlete skydiver3 = new Skydiver("Priit", "Kask", 34, "M", 183, 87);
        Arrays.asList(skydiver1, skydiver2, skydiver3).stream().forEach(a -> a.introduce());
//        skydiver1.introduce();
//        skydiver2.introduce();
//        skydiver3.introduce();

        System.out.println(skydiver1);
        System.out.println(skydiver2);
        System.out.println(skydiver3);

        Athlete runner1 = new Runner("Kaur", "Karu", 79, "M", 171, 68);
        Athlete runner2 = new Runner("Liina", "Mäger", 59, "N", 173, 71);
        Athlete runner3 = new Runner("Juhan", "Jänes", 24, "M", 185, 89);
        runner1.introduce();
        runner2.introduce();
        runner3.introduce();
        System.out.println(runner1);
        System.out.println(runner2);
        System.out.println(runner3);

        // Dogs...
        Dog turkishDog = new TurkishDog();
        Dog smallCzechDog = new SmallCzechDog();
        Dog armenianDog = new ArmenianDog();
        System.out.println("Turkish dog: ");
        turkishDog.bark();
        System.out.println("Small Czech dog: ");
        smallCzechDog.bark();
        System.out.println("Armenian dog: ");
        armenianDog.bark();
    }
}
