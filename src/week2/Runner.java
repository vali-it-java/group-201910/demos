package week2;

public class Runner extends Athlete {
    public Runner(String firstName, String lastName, int age, String gender, int height, double weight) {
        super(firstName, lastName, age, gender, height, weight);
    }

    @Override
    public String perform() {
        return "Running like wind...";
    }

    @Override
    public String getAthleteType() {
        return "Marathon runner";
    }


}
