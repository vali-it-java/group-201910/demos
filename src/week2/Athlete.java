package week2;

public abstract class Athlete {

    private String firstName;
    private String lastName;
    private int age;
    private String gender;
    private int height;
    private double weight;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public int getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Athlete(String firstName, String lastName, int age, String gender, int height, double weight) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
    }

    public abstract String perform();

    public abstract String getAthleteType();

    public void introduce() {
        System.out.printf("----- %s -----\n", this.getAthleteType());
        System.out.printf("Name: %s %s\n", this.getFirstName(), this.getLastName());
        System.out.printf("Age: %s, Weight %s, Height: %s\n", this.getAge(), this.getWeight(), this.getHeight());
        System.out.printf("Gender: %s\n", this.getGender());
    }

    @Override
    public String toString() {
        return "I am an athlete and I can do this..." + this.perform();
    }
}
