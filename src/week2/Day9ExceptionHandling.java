package week2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Day9ExceptionHandling {
    private static  List<String> fileLines = new ArrayList<>();

    public static void main(String[] args) {
        String filePath = "C:\\tmp\\nonexistentfile.txt";
        Scanner s = new Scanner(System.in);
        while(fileLines.size() == 0) {
            handleFileRead(filePath);
            if (fileLines.size() == 0) {
                System.out.println("Sisesta palun korrektne faili asukoht:");
                filePath = s.nextLine();
            }
        }

        System.out.println("Fail käes, nüüd saame oma tööga edasi minna.");
    }

    private static void handleFileRead(String filePath) {
        try {
            fileLines = readFile(filePath);
        } catch (IOException e) {
            System.out.println("Vabandust, aga faili ei leitud. Proovi palun uusesti!");
        } catch (NumberFormatException e) {

        }
//        catch (ArithmeticException e) {
//
//        }
        catch (ArrayIndexOutOfBoundsException e) {

        } catch(Exception e) {

        }
    }

    private static List<String> readFile(String filePath) throws IOException, ArithmeticException {
        return Files.readAllLines(Paths.get(filePath));
    }
}
