package week2.cryptor;

import java.util.List;

public class Decryptor extends Cryptor {

    public Decryptor(List<String> alphabet) {
        for(String line : alphabet) {
            String[] lineParts = line.split(", ");
            this.conversionMap.put(lineParts[1], lineParts[0]);
        }
    }
}
