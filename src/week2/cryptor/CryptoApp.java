package week2.cryptor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class CryptoApp {
    public static void main(String[] args) throws IOException {
        List<String> alphabet = Files.readAllLines(Paths.get(args[0]));

        Cryptor encryptor = new Encryptor(alphabet);
        Cryptor decryptor = new Decryptor(alphabet);
        Scanner scanner = new Scanner(System.in);

        System.out.println("KrüptoÄpp (ALL RIGHTS RESERVED)");
        while (true) {
            System.out.println("Mida soovid teha? [1 - välju, 2 - krüpteeri, 3 - dekrüpteeri]:");
            String command = scanner.nextLine();
            switch (command) {
                case "1":
                    System.out.println("Head aega!");
                    return;
                case "2":
                    System.out.println("Sisesta krüpteerimist vajav tekst:");
                    String text = scanner.nextLine();
                    System.out.println("Krüpteeritud tekst: " + encryptor.convert(text));
                    break;
                case "3":
                    System.out.println("Sisesta dekrüpteerimist vajav tekst:");
                    String textToDecrypt = scanner.nextLine();
                    System.out.println("Dekrüpteeritud tekst: " + decryptor.convert(textToDecrypt));
                    break;
                default:
                    System.out.println("Tundmatu käsklus!");
                    break;
            }
        }

    }


}
