package week2.cryptor;

import java.util.List;

public class Encryptor extends Cryptor {

    public Encryptor(List<String> alphabet) {
        for(String line : alphabet) {
            String[] lineParts = line.split(", ");
            this.conversionMap.put(lineParts[0], lineParts[1]);
        }
    }
}
