package week2.cryptor;

import java.util.HashMap;
import java.util.Map;

public abstract class Cryptor {

    protected Map<String, String> conversionMap = new HashMap<>();

    public String convert(String inputText) {
        String result = "";
        char[] inputTextCharacters = inputText.toUpperCase().toCharArray();
        //String.valueOf('A') --> "A"
        // encryptionMap.get("A") --> "Ü"

        // Lahendus 1 ("klassikaline" for)
        for(int i = 0; i < inputTextCharacters.length; i++) {
            String charStr = String.valueOf(inputTextCharacters[i]);
            result = result + this.conversionMap.get(charStr);
        }

//        // Lahendus 2 (mõnus ja mugav foreach)
//        for(char c : inputTextCharacters) {
//            String charStr = String.valueOf(c);
//            result = result + encryptionMap.get(charStr);
//        }

        return result;
    }
}
