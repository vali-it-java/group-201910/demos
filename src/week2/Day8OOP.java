package week2;

public class Day8OOP {
    public static void main(String[] args) {

        Car myCar = new Car();
        System.out.println(myCar instanceof Movable);
        System.out.println(myCar instanceof Car);

        Car someCar = new FerrariF355();
        System.out.println("Ferrari is Movable?");
        System.out.println(someCar instanceof Movable);
        System.out.println("Ferrari is Car?");
        System.out.println(someCar instanceof Car);
        System.out.println("Ferrari is Ford Fiesta?");
        System.out.println(someCar instanceof FordFiesta);
        System.out.println("Ferrari is Ferrari F355?");
        System.out.println(someCar instanceof FerrariF355);
        if (someCar instanceof FerrariF355) {
            System.out.println("Ferrari found!");
        }
    }
}
