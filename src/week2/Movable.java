package week2;

public interface Movable {

    void startEngine();

    void speedUp(int amount);

    void slowDown(int amount);

    int getFuelLeft();
}
