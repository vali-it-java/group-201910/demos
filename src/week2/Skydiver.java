package week2;

public class Skydiver extends Athlete {

    public Skydiver(String firstName, String lastName, int age, String gender, int height, double weight) {
        super(firstName, lastName, age, gender, height, weight);
    }

    @Override
    public String perform() {
        return "Falling from sky...";
    }

    @Override
    public String getAthleteType() {
        return "Skydiver";
    }
}
