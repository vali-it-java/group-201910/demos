package week2;

public class FerrariF355 extends Car {

    @Override
    public String getCarMake() {
        return "Ferrari";
    }

    @Override
    public String getCarModel() {
        return "F355";
    }

    @Override
    public int getMaxSpeed() {
        return 320;
    }

    @Override
    public void startEngine() {
        System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR!");
    }
}
