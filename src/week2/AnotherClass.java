package week2;

public class AnotherClass {
    public static void main(String[] args) {

        String code = "37605030299";
        boolean isCodeCorrect = Day06Methods.validatePersonalCode(code);

        printNumbers(25);
    }

    private static void printNumbers(int upperLimit) {
        System.out.println(upperLimit);
        if (upperLimit > 1) {
            printNumbers(--upperLimit);
        }
    }
}
