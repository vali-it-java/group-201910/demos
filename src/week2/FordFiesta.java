package week2;

public class FordFiesta extends Car {

    @Override
    public String getCarMake() {
        return "Ford";
    }

    @Override
    public String getCarModel() {
        return "Fiesta";
    }

    @Override
    public int getMaxSpeed() {
        return 165;
    }
}
