package week2.visitorcounterapp;

import java.util.Objects;

public class Visit implements Comparable<Visit> {
    private String date;
    private int count;

    public String getDate() {
        return date;
    }

    public int getCount() {
        return count;
    }

    public Visit(String visitText) {
        String[] parts = visitText.split(", ");
        this.date = parts[0];
        this.count = Integer.parseInt(parts[1]);
    }

    @Override
    public String toString() {
        return this.date + ":" + this.count;
    }

    @Override
    public int compareTo(Visit visit) {
        // < 0 --> objektid on õiges järjekorras
        // 0 --> objektid on võrdsed
        // > 0 --> objektid on vales järjekorras
        if(this.count < visit.count) {
            return -1;
        } else if (this.count > visit.count) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int hashCode() {
        int hash = 11;
        hash = 89 * hash + Objects.hashCode(this.date);
        hash = 89 * hash + Objects.hashCode(this.count);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) { // Samad objektid, ma olen alati võrdne iseendaga!
            return true;
        }
        if (obj == null) { // Olemasolev objekt ei võrdu kunagi mitteolemasolevaga!
            return false;
        }
        // Inimene ei ole võrdne delfiiniga!
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Visit otherVisit = (Visit) obj;
        return this.getDate().equals(otherVisit.getDate()) &&
                this.getCount() == otherVisit.getCount();
    }
}
