package week2.visitorcounterapp;

import java.util.Comparator;

public class VisitCompareInstruction implements Comparator<Visit> {
    @Override
    public int compare(Visit v1, Visit v2) {
        if (v1.getCount() < v2.getCount()) {
            return 1;
        } else if (v1.getCount() > v2.getCount()) {
            return -1;
        } else {
            return 0;
        }
    }
}
