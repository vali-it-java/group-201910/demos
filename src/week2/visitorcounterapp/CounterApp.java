package week2.visitorcounterapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CounterApp {

    private static List<Visit> visits = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        List<String> fileLines = Files.readAllLines(Paths.get(args[0]));

        for(int i = 0; i < fileLines.size(); i++) {
            visits.add(new Visit(fileLines.get(i)));
        }

//        // Default sorting based on Comparable interface
//        Collections.sort(visits);
//        Collections.reverse(visits);

        Collections.sort(visits, (v1, v2) -> {
            if (v1.getCount() < v2.getCount()) {
                return 1;
            } else if (v1.getCount() > v2.getCount()) {
                return -1;
            } else {
                return 0;
            }
        });

        Collections.sort(visits, new VisitCompareInstruction());

        System.out.println(visits);

        System.out.println("Minu interface inline implementatsioon:");
        MyTestInt mti = () -> 3;
//        MyTestInt mti = new MyIntImp();
        System.out.println(mti.getNumber());

        Visit myVisit1 = new Visit("2019-11-07, 5");
        Visit myVisit2 = new Visit("2019-11-07, 5");
        System.out.println("My Visit 1: " + myVisit1.hashCode());
        System.out.println("My Visit 2: " + myVisit2.hashCode());
        System.out.println("Kas visitid võrdsed? " + myVisit1.equals(myVisit2));

        String s1 = "tere";
        String s2 = "tere2";
        String s3 = "tere2";
        System.out.println("s1: " + s1.hashCode());
        System.out.println("s2: " + s2.hashCode());
        System.out.println("s3: " + s3.hashCode());
        System.out.println(myVisit1.equals(myVisit2));
    }

//    public static class MyIntImp implements MyTestInt {
//
//        @Override
//        public int getNumber() {
//            return 3;
//        }
//    }

    public static interface MyTestInt {
        int getNumber();
    }
}
