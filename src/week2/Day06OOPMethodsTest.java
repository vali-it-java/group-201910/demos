package week2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Day06OOPMethodsTest {

    @Test
    public void testDeriveBirthYear() {
        int birthYear = Day06Methods.deriveBirthYear("49403136515"); // 1994
//        assertTrue(birthYear == 1994);
        assertEquals(1994, birthYear, "The function did not return expected result.");
        assertTrue(Day06Methods.deriveBirthYear("1230000") == 1823);
        assertTrue(Day06Methods.deriveBirthYear("2230000") == 1823);
        assertTrue(Day06Methods.deriveBirthYear("6230000") == 2023);
    }

}
