package week2;

public class Employee {
    public String firstName;
    public String lastName;
    public String position;

    public void printEmployeeDetails() {
        System.out.printf("Employee details: %s %s / working as: %s\n",
                this.firstName, this.lastName, this.position);
    }
}
