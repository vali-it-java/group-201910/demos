package week2;

public class Person {

    private String code;

    public Person(String code) {
        this.code = code;
    }

    public int getBirthYear() {
        int firstDigit = Integer.parseInt(code.substring(0, 1));
        int birthYearDigits = Integer.parseInt(code.substring(1, 3));
        int centuryDigits = 0;
        switch (firstDigit) {
            case 1:
            case 2:
                centuryDigits = 1800;
                break;
            case 3:
            case 4:
                centuryDigits = 1900;
                break;
            case 5:
            case 6:
                centuryDigits = 2000;
                break;
        }
        return centuryDigits + birthYearDigits;
    }

    public int getBirthMonth() {
        return Integer.parseInt(this.code.substring(3, 5));
    }

    public int getBirthDayOfMonth() {
        return Integer.parseInt(this.code.substring(5, 7));
    }

    public String getGender() {
        int firstDigit = Integer.parseInt(code.substring(0, 1));
        return firstDigit % 2 == 0 ? "F" : "M";
    }
}
