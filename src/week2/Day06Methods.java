package week2;

import java.util.ArrayList;
import java.util.List;

public class Day06Methods {
    public static String test = "aaa";

    public static void main(String[] args) {
        double finalPrice = addVat(23.99);
        System.out.println(finalPrice);
        saveToDatabase("Testtoode");
        printHello("Marek");

        System.out.println("Inimene isikukoodiga 49403136515" +
                " on sündinud aastal " + deriveBirthYear("49403136515"));
        System.out.println("Inimene isikukoodiga 37605030299" +
                " on sündinud aastal " + deriveBirthYear("37605030299"));
        System.out.println("Inimene isikukoodiga 61107121760" +
                " on sündinud aastal " + deriveBirthYear("61107121760"));
        String personalCodeVeryOldPerson = "11107121760";
        System.out.println("Inimene isikukoodiga " + personalCodeVeryOldPerson +
                " on sündinud aastal " + deriveBirthYear(personalCodeVeryOldPerson));

        // Exercise 1
        ex1(3);

        // Exercise 2
        ex2("AAA", "TTT");

        // Exercise 6
        String gender = deriveGender("37605030299");
        System.out.println("Inimene isikukoodiga 37605030299 on " + gender);

        List<String> myTexts = new ArrayList<>();
        int myNumber = 5;
        System.out.println("Enne funktsiooni väljakutsumist: ");
        System.out.println(myNumber);
        System.out.println(myTexts);
        changeInputParameterValues(myNumber, myTexts);
        System.out.println("Pärast funktsiooni väljakutsumist: ");
        System.out.println(myNumber);
        System.out.println(myTexts);

        // Exercise 8
        boolean isCodeCorrect = validatePersonalCode("37605030299");
        System.out.println("Kas isikukood on korrektne? " + isCodeCorrect);
    }

    public static void changeInputParameterValues(int a, List<String> texts) {
        a = 6;
        texts.add("Mingi tekst");
    }

    public static double addVat(double price) {
        return 1.2 * price;
    }

    public static void saveToDatabase(String productName) {
        // Logic for saving the string to database...
    }

    public static void printHello(final String name) {
        System.out.println("Hello, " + name);
    }

    public static void printHello(String name, String anotherName) {
        System.out.println("Hello you both, " + name + " and " + anotherName);
    }

    public static int deriveBirthYear(String code) { // code: 49403136515
        int firstDigit = Integer.parseInt(code.substring(0, 1)); // 4
        int birthYearDigits = Integer.parseInt(code.substring(1, 3)); // 94

        // 1, 2 --> 1800 + ...
        // 3, 4 --> 1900 + ...
        // 5, 6 --> 2000 + ...
        int centuryDigits = 0;
        switch (firstDigit) {
            case 1:
            case 2:
                centuryDigits = 1800;
                break;
            case 3:
            case 4:
                centuryDigits = 1900;
                break;
            case 5:
            case 6:
                centuryDigits = 2000;
                break;
        }

        return centuryDigits + birthYearDigits;
    }

    // Exercise 1
    public static boolean ex1(int i) {
        return true;
    }

    // Exercise 2
    public static void ex2(String s1, String s2) {
    }

    // Exercise 4
    public static int[] ex4(int a, int b, boolean c) {
        return new int[]{};
    }

    // Exercise 6
    public static String deriveGender(String code) {
        int firstDigit = Integer.parseInt(code.substring(0, 1));
        return firstDigit % 2 == 0 ? "F" : "M";
//        switch (firstDigit) {
//            case 1:
//            case 3:
//            case 5:
//                return "M";
//            case 2:
//            case 4:
//            case 6:
//                return "F";
//            default:
//                return "X";
//        }

    }

    // Exercise 8
    public static boolean validatePersonalCode(String code) {
        if (code == null || code.length() != 11) {
            return false;
        }

        int[] codeDigits = new int[11];
        for (int i = 0; i < codeDigits.length; i++) {
            codeDigits[i] = Integer.parseInt("" + code.charAt(i));
        }

        int sum = 0;
        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        for(int i = 0; i < weights1.length; i++) {
            sum = sum + weights1[i] * codeDigits[i];
        }

        int checkDigit = sum % 11;
        if (checkDigit != 10) {
            return checkDigit == codeDigits[10];
        } else {
            sum = 0;
            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            for(int i = 0; i < weights2.length; i++) {
                sum = sum + weights2[i] * codeDigits[i];
            }

            checkDigit = sum % 11;
            if (checkDigit == 10) {
                return codeDigits[10] == 0;
            } else {
                return codeDigits[10] == checkDigit;
            }
        }
    }
}
