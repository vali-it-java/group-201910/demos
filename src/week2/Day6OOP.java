package week2;

public class Day6OOP {
    public String testProperty;

    public static void main(String[] args) {

        // Exercise 2 (OOP)
        CountryInfo estonia = new CountryInfo();
        estonia.countryName = "Eesti";
        estonia.capital = "Tallinn";
        estonia.primeMinister = "Jüri Ratas";
        estonia.languages = new String[]{"Eesti", "Vene", "Soome"};

        CountryInfo finland = new CountryInfo("Soome", "Helsinki",
                "Antti Rinne", new String[]{"Soome", "Rootsi"});

        CountryInfo germany = new CountryInfo();

        CountryInfo[] countries = {estonia, finland, germany};

//        for (int i = 0; i < countries.length; i++) {
//            System.out.printf("Name: %s, capital: %s, prime minister: %s:\n",
//                    countries[i].countryName, countries[i].capital, countries[i].primeMinister);
//            for (int j = 0; j < countries[i].languages.length; j++) {
//                System.out.println("\t" + countries[i].languages[j]);
//            }
//        }

        for(int i = 0; i < countries.length; i++) {
            System.out.println(countries[i]);
        }

        String code = "4940313651";
        Person person = new Person(code);
        System.out.println("Persoon isikukoodiga " + code);
        System.out.println("Sugu: " + person.getGender());
        System.out.println("Sünniaasta: " + person.getBirthYear());
        System.out.println("Sünnikuu: " + person.getBirthMonth());
        System.out.println("Sünnikuupäev: " + person.getBirthDayOfMonth());
    }

}
