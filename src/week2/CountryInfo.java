package week2;

public class CountryInfo {
    public String countryName = "COUNTRY NAME";
    public String capital = "CAPITAL";
    public String primeMinister = "PRIME MINISTER";
    public String[] languages = new String[0];

    public CountryInfo() {

    }

    public CountryInfo(String countryName, String capital, String primeMinister, String[] languages) {
        this.countryName = countryName;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;
    }

    @Override
    public String toString() {
        String countryInfoText = String.format("RIIK: [Nimi: %s, Pealinn: %s, Peaminister: %s:\n",
                this.countryName, this.capital, this.primeMinister);
        for(int i = 0; i < this.languages.length; i++) {
            countryInfoText = countryInfoText + "\t" + this.languages[i] + "\n";
        }
        countryInfoText = countryInfoText + "]";
        return countryInfoText;
    }
}
