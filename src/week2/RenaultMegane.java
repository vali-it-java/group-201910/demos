package week2;

public class RenaultMegane extends Car {

    @Override
    public String getCarMake() {
        return "Renault";
    }

    @Override
    public String getCarModel() {
        return "Megane";
    }
}
