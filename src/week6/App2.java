package week6;

public class App2 {
    public static void main(String[] args) {

        System.out.println(replaceSpacesWithNewline("See on mingi teks, kus on palju sõnu!"));
        System.out.println(tripleCharactersInWord("Vali IT!"));
        System.out.println(censorText("Auto sõidab maanteel, vasti jõuab kohale!"));
    }

    private static String replaceSpacesWithNewline(String inputText) {
        return inputText.replaceAll(" ", "\n");
    }

    private static String tripleCharactersInWord(String text) {
        String result = "";
        for(int i = 0; i < text.length(); i++) {
            result += String.valueOf(text.charAt(i)).repeat(3);
        }
        return result;
    }

    private static String censorText(String text) {
        return text.replaceAll("[^!,\\.\\?\\s\\:\\;]", "#");
    }
}
