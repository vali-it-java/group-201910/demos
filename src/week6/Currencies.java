package week6;

public class Currencies {
    public static void main(String[] args) {

        UsdEurCurrencyConverter usdEurConverter = new UsdEurCurrencyConverter();
        double valueInEur = usdEurConverter.toEuros(120.0);
        System.out.printf("%.2f USA dollarit on %.2f eurot\n", 120.0, valueInEur);

        double valueInUsd = usdEurConverter.fromEuros(342.69);
        System.out.printf("%.2f eurot on %.2f USA dollarit\n", 342.69, valueInUsd);

        GbpEurCurrencyConverter gbpEurConverter = new GbpEurCurrencyConverter();
        double valueInEur2 = gbpEurConverter.toEuros(120.0);
        System.out.printf("%.2f Briti naela on %.2f eurot\n", 120.0, valueInEur2);

        double valueInGbp = gbpEurConverter.fromEuros(342.69);
        System.out.printf("%.2f eurot on %.2f Briti naela\n", 342.69, valueInGbp);

    }

    public static abstract class GenericCurrencyConverter {
        public abstract double getExchangeRate();

        public double toEuros(double foreignCurrencyValue) {
            return foreignCurrencyValue / getExchangeRate();
        }

        public double fromEuros(double valueInEuros) {
            return valueInEuros * getExchangeRate();
        }
    }

    public static class UsdEurCurrencyConverter extends GenericCurrencyConverter {

        @Override
        public double getExchangeRate() {
            return 1.0982;
        }
    }

    public static class GbpEurCurrencyConverter extends GenericCurrencyConverter {

        @Override
        public double getExchangeRate() {
            return 0.8523;
        }
    }
}
