package week6;

public class App {
    public static void main(String[] args) {

        int seconds = 1_000_000_000;
        System.out.printf("Periood %d sekundit: %s\n", seconds, buildPeriod(seconds));
        System.out.printf("Periood %d sekundit: %s\n", 2800, buildPeriod(2800));
        System.out.printf("Periood %d sekundit: %s\n", 86400, buildPeriod(86400));
        System.out.printf("Periood %d sekundit: %s\n", 36001, buildPeriod(36001));
    }

    private static Period buildPeriod(int seconds) {
        int years = seconds / (60 * 60 * 24 * 30 * 12);
        int leftofer = seconds % (60 * 60 * 24 * 30 * 12);
        int months = leftofer / (60 * 60 * 24 * 30);
        leftofer = leftofer % (60 * 60 * 24 * 30);
        int days = leftofer / (60 * 60 * 24);
        leftofer = leftofer % (60 * 60 * 24);
        int hours = leftofer / (60 * 60);
        leftofer = leftofer % (60 * 60);
        int minutes = leftofer / 60;
        leftofer = leftofer % 60;
        return new Period(years, months, days, hours, minutes, leftofer);
    }

    public static class Period {

        private int years;
        private int months;
        private int days;
        private int hours;
        private int minutes;
        private int seconds;

        public Period(int years, int months, int days, int hours, int minutes, int seconds) {
            this.years = years;
            this.months = months;
            this.days = days;
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }

        public int getYears() {
            return years;
        }

        public int getMonths() {
            return months;
        }

        public int getDays() {
            return days;
        }

        public int getHours() {
            return hours;
        }

        public int getMinutes() {
            return minutes;
        }

        public int getSeconds() {
            return seconds;
        }

        @Override
        public String toString() {
            return String.format("%d aastat, %d kuud, %d päeva, %d tundi, %d minutit, %d sekundit",
                    years, months, days, hours, minutes, seconds);
        }
    }
}
