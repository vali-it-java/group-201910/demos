package week5;

public class Ex1 {
    public static void main(String[] args) {

        System.out.printf("Toode hinnaga %s maksaks ilma käibemaksuta %s\n",
                100.0, removeVat(100.0));

        System.out.printf("Fraas '%s' on palindrome? %s\n",
                "Aias sadas saia", isPlanindrome("Aias sadas saia"));
        System.out.printf("Fraas '%s' on palindrome? %s\n",
                "Aias sadas saia.", isPlanindrome("Aias sadas saia."));

        double bmi = calculateBmi(75, 180);
        System.out.printf("Inimene pikkusega 180cm ja kaaluga 75kg omab kehamassiindeksit %s ja kuulub kategooriasse '%s'.\n",
                bmi, deriveBmiDescription(bmi));
    }

    private static double removeVat(double grossPrice) {
        double netPrice = grossPrice / 1.2;
        netPrice = Math.round(netPrice * 100.0) / 100.0;
        return netPrice;
    }

    private static boolean isPlanindrome(String text) {
        String reversedText = "";

        // Variant 1
//        for(int i = 0; i < text.length(); i++) {
//            reversedText = text.charAt(i) + reversedText;
//        }

        // Variant 2
        StringBuilder sb = new StringBuilder(text);
        reversedText = sb.reverse().toString();

        return text.equalsIgnoreCase(reversedText);
    }

    private static double calculateBmi(double weightInKilos, double heightInCm) {
        double heightInM = heightInCm / 100.0;
        double bmi = weightInKilos / (heightInM * heightInM);
        bmi = Math.round(bmi * 100.0) / 100.0;
        return bmi;
    }

    private static String deriveBmiDescription(double bmi) {
        if (bmi < 16) {
            return "Tervisele ohtlik alakaal";
        } else if (bmi < 19) {
            return "Alakaal";
        } else if (bmi < 25) {
            return "Normaalkaal";
        } else if (bmi < 30) {
            return "Ülekaal";
        } else if (bmi < 35) {
            return "Rasvumine";
        } else if (bmi < 40) {
            return "Tugev rasvumine";
        } else {
            return "Tervisele ohtlik rasvumine";
        }
    }
}
