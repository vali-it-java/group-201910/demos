package week1;

public class Day03Arrays {

    public static final double VAT_RATE = 20;
    public static String something = "XXX";

    public static void main(String[] args) {
        // Exercise 7
        int[] myIntArray;

        // Variant 1
        myIntArray = new int[5];
        myIntArray[0] = 1;
        myIntArray[1] = 2;
        myIntArray[2] = 3;
        myIntArray[3] = 4;
        myIntArray[4] = 5;

        // Variant 2
        myIntArray = new int[]{1, 2, 3, 4, 5};

        System.out.println("Esimene element: " + myIntArray[0]);
        System.out.println("Kolmas element: " + myIntArray[2]);
        System.out.println("Viimane element: " + myIntArray[myIntArray.length - 1]);

        // Exercise 8
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris", "New York"};
        for(String city : cities) {
            System.out.println(city);
        }
        for(int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);
        }

        // Exercise 9
        int[][] my2DimArray = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 0}
        };

        for(int[] n : my2DimArray) {
            for(int x : n) {
                System.out.println(x);
            }
        }

        for(int a = 0; a < my2DimArray.length; a++) {
            for (int b = 0; b < my2DimArray[a].length; b++) {
                System.out.println(my2DimArray[a][b]);
            }
        }

        // Exercise 10
        // Variant 1
        String[][] countryCities = {
                {"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"}
        };

        // Variant 2
        String[][] countryCities2 = new String[3][4];
        countryCities2[0][0] = "Tallinn";
        countryCities2[0][1] = "Tartu";
        countryCities2[0][2] = "Valga";
        countryCities2[0][3] = "Võru";

        countryCities2[1][0] = "Stockholm";
        countryCities2[1][1] = "Uppsala";
        countryCities2[1][2] = "Lund";
        countryCities2[1][3] = "Köping";

        countryCities2[2][0] = "Helsinki";
        countryCities2[2][1] = "Espoo";
        countryCities2[2][2] = "Hanko";
        countryCities2[2][3] = "Jämsä";


    }
}
