package week1;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Day03LoopExercises {
    public static void main(String[] args) {

        // Exercise 11
        int number = 1;
        while (number <= 100) {
            System.out.print(number + "\n");
            number++;
        }

        // Exercise 12
        for (int i = 1; i <= 100; i++) {
            System.out.println(i);
        }

        // Exercise 13
        int[] digits = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int digit : digits) {
            System.out.println(digit);
        }

        // Exercise 14

        // Variant 1
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

        // Variant 2
        for (int i = 3; i <= 100; i += 3) {
            System.out.println(i);
        }

        // Exercise 15
        // "text".length
        // "text".substring() // "tere".substring(1, 3); --> er
        // "Sun, Metsatöll, Queen, Metallica, ".substring(..., ...)
        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica", "U2"};
        // Faas 1: "Sun, Metsatöll, Queen, Metallica, U2, "
        String bandsText = "";
        // Eest poolt tagapoole
        for(int i = 0; i < bands.length; i++) {
            bandsText = bandsText + bands[i] + ", ";
        }
//        // Tagant poolt ettepoole
//        for(int i = bands.length - 1; i >= 0; i--) {
//            bandsText = bandsText + bands[i] + ", ";
//        }
        bandsText = bandsText.substring(0, bandsText.length() - 2);
        System.out.println(bandsText);

        String result2 = String.join(", ", bands);
        System.out.println(result2);

        // Exercise 16
        String result3 = "";
        for(String band : bands) {
            result3 = band + ", " + result3;
        }
        result3 = result3.substring(0, result3.length() - 2);
        System.out.println(result3);

        // Ülesanne 17
        // args: {"4", "5", "6"}
        // Step 1: prindi välja args massiiv, iga parameeter eraldi real. Kasuta tsüklit.

        String numbersAsText = "";
        for(int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "0":
                    numbersAsText = numbersAsText + " Null";
                    break;
                case "1":
                    numbersAsText = numbersAsText + " Üks";
                    break;
                case "2":
                    numbersAsText = numbersAsText + " Kaks";
                    break;
                case "3":
                    numbersAsText = numbersAsText + " Kolm";
                    break;
                case "4":
                    numbersAsText = numbersAsText + " Neli";
                    break;
                case "5":
                    numbersAsText = numbersAsText + " Viis";
                    break;
                case "6":
                    numbersAsText = numbersAsText + " Kuus";
                    break;
                case "7":
                    numbersAsText = numbersAsText + " Seitse";
                    break;
                case "8":
                    numbersAsText = numbersAsText + " Kaheksa";
                    break;
                case "9":
                    numbersAsText = numbersAsText + " Üheksa";
                    break;
                default:
                    // Mingi jama
            }
        }
        System.out.println(numbersAsText.substring(1));

        // Ülesanne 18
        double randomNumber = 0;
        do {
            System.out.println("tere");
            randomNumber = Math.random();
        } while(randomNumber < 0.5);

        for(int i = 0; i < bands.length; i++) {

        }

        String text = "Mingid t2hed";

        for(char t : text.toCharArray()) {

        }

        int bandNumber = 0;
        while(bandNumber < bands.length) {
            System.out.println(bands[bandNumber]);
            bandNumber++;
        }

        bandNumber = 0;
        do {
            System.out.println(bands[bandNumber]);
            bandNumber++;
        } while(bandNumber < bands.length);
    }
}
