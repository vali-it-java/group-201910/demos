package week1;

import java.math.BigDecimal;

public class ProgrammingBasicsExercises {
    public static void main(String[] args) {

        // Exercise 1

        double myDouble = 456.78;
        float myFloat = 456.78F;

        String myText1 = "test";

        int[] myNumbers = {5, 91, 304, 405};

        // Semantiliselt samaväärne eelmise operatsiooniga.
        myNumbers = new int[4];
        myNumbers[0] = 5;
        myNumbers[1] = 91;
        myNumbers[2] = 304;
        myNumbers[3] = 405;

        double[] myDoubleNumbers = {56.7, 45.8, 91.2};

        double myTestVal = 0;
        String[] myValues = {"see on esimene väärtus", "67", "58.92"};
        double myLastValue = Double.parseDouble(myValues[2]);

        System.out.println(myTestVal);

        BigDecimal myBigDecimal = new BigDecimal("7676868683452352345324534534523453245234523452345234523452345");
        //myBigDecimal = myBigDecimal.add(new BigDecimal("1"));

        // Exercise 2

        String city = "Berlin";
        if (city.equalsIgnoreCase("Milano")) {
            System.out.println("Ilm on soe.");
        } else {
            System.out.println("Ilm polegi kõige tähtsam.");
        }

        // Exercise 3

        int grade = Integer.parseInt(args[0]);

        if (grade == 1) {
            System.out.println("Nõrk");
        } else if (grade == 2) {
            System.out.println("Mitterahuldav");
        } else if (grade == 3) {
            System.out.println("Rahuldav");
        } else if (grade == 4) {
            System.out.println("Hea");
        } else if (grade == 5) {
            System.out.println("Väga hea".toUpperCase());
        } else {
            System.out.println("Viga: ebakorrektne sisend");
        }

        // Exercise 4
        switch (grade) {
            case 1:
                System.out.println("Nõrk");
                break;
            case 2:
                System.out.println("Mitterahuldav");
                break;
            case 3:
                System.out.println("Rahuldav");
                break;
            case 4:
                System.out.println("Hea");
                break;
            case 5:
                System.out.println("Väga hea");
                break;
            default:
                System.out.println("Viga: ebakorrektne sisend");
        }

        // Exercise 5
        int age = 100;
        String ageGroup = age > 100 ? "Vana" : "Noor";
        System.out.println("Vanusegrupp: " + ageGroup);

        // Exercise 6
        String ageGroup2 = (age > 100) ? ("Vana") : ((age == 100) ? ("Peaaegu vana") : ("Noor"));
        System.out.println("Vanusegrupp: " + ageGroup2);

        String ageText = String.valueOf(age);
        System.out.println(ageText);
    }
}
