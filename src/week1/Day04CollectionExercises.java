package week1;

import java.util.*;
import java.util.stream.Stream;

public class Day04CollectionExercises {
    public static void main(String[] args) {

        // Ülesanne 19
        List<String> citiesList = new ArrayList<>();
        citiesList.add("Tartu");
        citiesList.add("Kohila");
        citiesList.add("Baden-Baden");
        citiesList.add("Madrid");
        citiesList.add("Saue");
        System.out.println("Esimene linn: " + citiesList.get(0));
        System.out.println("Kolmas linn: " + citiesList.get(2));
        System.out.println("Viimane linn: " + citiesList.get(citiesList.size() - 1));

        // Ülesanne 20
        Queue<String> namesQueue = new LinkedList<>();
        namesQueue.add("Axel");
        namesQueue.add("Pedro");
        namesQueue.add("Janne");
        namesQueue.add("Reet");
        namesQueue.add("Rita");

//        System.out.println(namesQueue.remove());
//        System.out.println(namesQueue.remove());
//        System.out.println(namesQueue);

        System.out.println(namesQueue.peek());
        System.out.println(namesQueue.peek());
        while(!namesQueue.isEmpty()) {
            System.out.println(namesQueue.remove());
        }
//        for(String name : namesQueue) {
//            System.out.println("Name: " + name);
//        }

        Deque<Integer> stack = new ArrayDeque<>();
        stack.add(1);
        stack.add(2);
        System.out.println(stack.removeLast());
        System.out.println(stack.removeLast());

        // Ülesanne 21
        Set<String> coutries = new TreeSet<>();
        coutries.add("Eesti");
        coutries.add("Läti");
        coutries.add("Leedu");
        coutries.add("Soome");
        coutries.add("Rootsi");
        coutries.add("USA");
        coutries.add("Kanada");
//        for(String country : coutries) {
//            System.out.println(country);
//        }
//        coutries.forEach(System.out::println);
        coutries.forEach(x -> System.out.println(x));

        // Ülesanne 22
        Map<String, String[]> countryMap = new HashMap<>();

        String[] estoniaCities = new String[4];
        estoniaCities[0] = "Tallinn";
        estoniaCities[1] = "Tartu";
        estoniaCities[2] = "Valga";
        estoniaCities[3] = "Võru";
        countryMap.put("Eesti", estoniaCities);
        countryMap.put("Rootsi", new String[]{ "Stockholm", "Uppsala", "Lund", "Köping" });
        countryMap.put("Soome", new String[]{ "Helsinki", "Espoo", "Hanko", "Jämsä" });

//        String[] countryNames = countryMap.keySet().toArray(String[]::new);
//        for(int i = 0; i < countryNames.length; i++) {
//            System.out.println("Country: " + countryNames[i]);
//            System.out.println("Cities:");
//            String[] currentCountryCities = countryMap.get(countryNames[i]);
//            for(int j = 0; j < currentCountryCities.length; j++) {
//                System.out.println("\t" + currentCountryCities[j]);
//            }
//        }

        for(Map.Entry<String, String[]> country : countryMap.entrySet()) {
            System.out.println("Country: " + country.getKey());
            System.out.println("Cities:");
            for(String city : country.getValue()) {
                System.out.println("\t" + city);
            }
        }

        countryMap.forEach((name, cities) -> {
            System.out.println("Country: " + name);
            System.out.println("Cities:");
            Stream.of(cities).forEach(city -> System.out.println("\t" + city));
        });
    }
}






