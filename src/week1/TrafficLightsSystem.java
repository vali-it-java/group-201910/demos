package week1;

public class TrafficLightsSystem {

    public static void main(String[] args) {
        String color = args[0].toUpperCase();

        if (color.equals("RED")) {
            System.out.println("Driver has to stop car and wait for green light.");
        } else if (color.equals("YELLOW")) {
            System.out.println("Driver has to be ready to stop the car or to start driving.");
        } else if (color.equals("GREEN")) {
            System.out.println("Driver can drive a car.");
        } else {
            System.out.println("ERROR: unknown input!");
        }

//        switch(color) {
//            case "RED":
//                System.out.println("Driver has to stop car and wait for green light.");
//                break;
//            case "YELLOW":
//                System.out.println("Driver has to be ready to stop the car or to start driving.");
//                break;
//            case "GREEN":
//                System.out.println("Driver can drive a car.");
//                break;
//            default:
//                System.out.println("ERROR: unknown input!");
//        }
    }
}
