package week1;

public class StringProcessingExercises {
    public static void main(String[] args) {

        // Ex 1:

        System.out.println("Hello, World!");

        System.out.println("Hello, \"World\"!");

        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren't funny\".");

        System.out.println("Kui liita kokku sõned \"See on teksti esimene pool  \" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\".");

        System.out.println("Elu on ilus");

        System.out.println("Elu on 'ilus'");
        System.out.println("Elu on \'ilus\'");
//        System.out.println('\'');

        System.out.println("Elu on \"ilus\"");

        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");

        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");

        System.out.println("'Kolm' - kolm, \'neli\' - neli, \"viis\" - viis");

        // Ex 2:

        String tallinnPopulation = "450 000";

        // Variant 1:
        System.out.println("Tallinnas elab " + tallinnPopulation + " inimest");

        // Variant 2:
        System.out.println(String.format("Tallinnas elab %s inimest", tallinnPopulation));

        int populationOfTallinn = 450_000;
//        float test = 1.2f;
        System.out.println(String.format("Tallinnas elab %s inimest", populationOfTallinn));

        // Ex 3:
        String bookTitle = "Rehepapp";
        System.out.println("Raamatu \"" + bookTitle + "\" autor on Andrus Kivirähk.");

        // Ex 4:
        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Mars";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        int planetCount = 8;

        // Variant 1:
        System.out.println(planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " +
                planet5 + ", " + planet6 + ", " + planet7 + ", " + planet8 +
                " on Päikesesüsteemi " + planetCount + " planeeti.");

        // Variant 2:
        System.out.printf("%s, %s, %s, %s, %s, %s, %s, %s on Päikesesüsteemi %d planeeti.",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount);
    }
}
