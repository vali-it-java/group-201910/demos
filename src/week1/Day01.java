package week1;

public class Day01 {
    public static void main(String[] args) {

        byte b1 = (byte)777;
        System.out.println("Minu byte: " + b1);

        short s1 = 777;
        System.out.println("Minu short: " + s1);

        char charA = 'A';
        System.out.println("Minu char: " + (short)charA);

        char charA2 = 65;
        System.out.println("Minu char2: " + charA2);

        int myPrimitiveInt = 5;
        Integer myComplexInt = 5;
        myComplexInt = Integer.parseInt("789");
        Short myComplexShort = Short.parseShort("789");
        System.out.println(Integer.toBinaryString(777));

        Integer i11 = 11;
        Integer i12 = 12;
        int iSum = i11 + i12;

        // Avaldised
        int myNumber2 = (4 + 5) / 3 * 5;

//        boolean myTest = 4 == "4";
        int myInt13 = 13;
        myInt13 = myInt13 + 1; // Tulemus: myInt on 14
        myInt13++;
        ++myInt13;

        int myInt14 = 8;
        System.out.println("My integer: " + ++myInt14);

        int myInt15 = 15;
        myInt15 = myInt15 + 5;
        myInt15 += 5; // Semantiliselt identne eelmise reaga.

//        myInt15 /= 3;

        /*

            (5 + 5) * 2 =
            5 - 5 + 5 =
            5 * 5 / 2 =
            5 / 5 * 2 =

            1 + 0 = 1 // true + false = true / true || false == true
            1 + 1 = 1 // true + true == true

            1 * 0 * 1 * 1 * 1 = 0 // true && false == false

            Liitmine - loogiline OR (Java maailmas: a || b)
            Korrutamine / loogiline AND (Java maailmas: a && b)

            false || false || false || true --> true
            false && false && false && true --> false

            false || ture && false --> false

            OR ehk || - madala prioriteediga operaator
            AND ehk && - kõrge prioriteediga operaator

            Arvuliste muutujate operaatorid veelgi kõrgema prioriteediga!

        */

        boolean myBoolValue1;
        myBoolValue1 = true;
        myBoolValue1 = 5 > 3 && 4 < 9 || (1 == 2 || 2 >= 5);

    }
}
