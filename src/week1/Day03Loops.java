package week1;

import java.util.Scanner;

public class Day03Loops {
    public static void main(String[] args) {

        // For tsükkel

        for (int i = 0; i <= 3; i++) {
            System.out.println("OK " + i);
        }

        // Lõputu tsükkel Java VMi hulluksajamiseks.
//        for(;;);


        // Juhtmuutuja defineerimine ja suurendamine väljaspool tsükli päist.
        int num = 5;
        for (; num < 6; ) {
            System.out.println("Tsykkel: " + num);
            num++;
        }

        for (int u = 100; u > 0; u = u - 4 * 8) {
            System.out.println("U väärtus: " + u);
        }

        String[] employees = {"Rain", "Raiko", "Heidi", "Thomas"};
        for (int i = 0; i < employees.length; i++) {
            System.out.println("Töötaja: " + employees[i]);
        }

        // Foreach

        String[] employees2 = {"Rain", "Raiko", "Heidi", "Thomas"};

        for (String abc : employees2) {
            System.out.println("Töötaja nimi: " + abc);
        }

        // While
//        while(true);

        boolean test = true;
        while (test) {
            System.out.println("Tsykkel...");
            double randomNumber = Math.random();
            if (randomNumber > 0.5) {
                test = false;
            }
        }

        String[] employees3 = {"Rain", "Raiko", "Heidi", "Thomas"};
        int index = 0;
        while(index < employees3.length) {
            System.out.println("Töötaja: " + employees3[index]);
            index++;
        }

        // Do-while
//        Scanner scanner = new Scanner(System.in);
//        int number = 0;
//        do {
//            System.out.println("Sisesta number, mis suurem, kui 678 ja jagub kolmega:");
//            number = Integer.parseInt(scanner.nextLine());
//            if (number > 678 && number % 3 == 0)  {
//                System.out.println("Number sobib!");
//            }
//        } while(!(number > 678 && number % 3 == 0));

        // Continue keyword
        String[] employees4 = {"Rain", "Raiko", "Heidi", "Thomas"};
        for(String name : employees4) {
            if (name.startsWith("H")) {
                // Tee midagi tarka.
            } else {
                continue;
            }
            // Tee midagi veel...
            // Ja midagi veel....
        }

        // Break keyword
        for (int i = 1; i < 100; i++) {
            if (i % 42 == 0) {
                System.out.println("Tuvastatud fataalne tingimus. Tsüklit jätkata ei saa.");
                continue; // Seda konkreetset tsyklikorda j2tkata ei saa, alusame j2rgmise tsyklikorraga...
//                break; // Lõpetame tsükli ja jätkame programmi täitmist pärast tsüklit.
            }
            // Tee midagi
            System.out.println("Number " + i);
        }
    }
}
