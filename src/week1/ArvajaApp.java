package week1;

import javafx.util.Pair;

import java.util.*;

public class ArvajaApp {
    private static final int RANGE = 10;

    public static void main(String[] args) {
        List<MyPair> board = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            int randomNumber = (int) (Math.random() * RANGE + 1);

            System.out.print("Sisesta oma nimi: ");
            String userName = scanner.nextLine();

            int guessCount = handleGuessing(scanner, randomNumber);
            System.out.println("Õige vastus! Sul kulus " + guessCount + " korda, et ära arvata");
            addResultToBoard(board, userName, guessCount);

            if (handleFlow(board, scanner)) {
                return;
            }
        }
    }

    private static boolean handleFlow(List<MyPair> board, Scanner scanner) {
        int userNumber;
        boolean continueCycle = true;
        while (continueCycle) {
            System.out.println("Vali tegevus (1 - välju programmist, 2 - prindi logi, muu number - uus mäng):");
            userNumber = Integer.parseInt(scanner.nextLine());
            if (userNumber == 1) {
                return true;
            } else if (userNumber == 2) {
                printRestuts(board);
            } else {
                continueCycle = false;
            }
        }
        return false;
    }

    private static void printRestuts(List<MyPair> board) {
        System.out.println("------------------------------");
        System.out.println("TULEMUSED");
        System.out.println("------------------------------");
        for (int i = 0; i < board.size(); i++) {
            MyPair logItem = board.get(i);
            System.out.println("Nimi: " + logItem.userName);
            System.out.println("Kordi: " + logItem.guessCount);
        }
        System.out.println("------------------------------");
    }

    private static void addResultToBoard(List<MyPair> board, String userName, int guessCount) {
        // Tegeleme logikirje lisamisega...
        MyPair result = new MyPair();
        result.userName = userName;
        result.guessCount = guessCount;
        board.add(result);
        Collections.sort(board, (p1, p2) -> {
            // kui vastus on negatiivne, siis ei pea ringi keerama
            // kui vastus on 0, siis ei pea ringi keerama
            // kui vastus on positiivne, tuleb elementide järjekord vahetada

            // p1: Marek 5
            // p2: Teet 3

            // Variant 1:
//                return p1.guessCount - p2.guessCount;

            // Variant 2:
            if (p1.guessCount < p2.guessCount) {
                return -1;
            } else if (p1.guessCount == p2.guessCount) {
                return 0;
            } else {
                return 1;
            }
        });
    }

    private static int handleGuessing(Scanner scanner, int randomNumber) {
        int userNumber = 0;
        int guessCount = 0;
        do {
            guessCount++;
            System.out.println("Sisesta number vahemikus 1 kuni " + RANGE + ":");
            userNumber = Integer.parseInt(scanner.nextLine());
            if (userNumber > randomNumber) {
                System.out.println("Liiga suur!");
            } else if (userNumber < randomNumber) {
                System.out.println("Liiga väike!");
            }
        } while (randomNumber != userNumber);
        return guessCount;
    }

    public static class MyPair {
        public String userName;
        public int guessCount;
    }
}
