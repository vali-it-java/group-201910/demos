package week1;

import java.util.Scanner;

public class Day03Strings {

    public static void main(String[] args) {

        // StringBuffer, StringBulder

        String text1 = "Tere";
        text1 = text1 + ", tere!";
//        text1 = "Tere, tere!";

        // String buffer
//        StringBuilder sb = new StringBuilder();
        StringBuffer sb = new StringBuffer();
        sb.append("Tere");
        sb.append(", tere!");
        String text2 = sb.toString();
        System.out.println(text2);

        // Split

        String names = "Maarika, Malle, Paul, Kalle";
        String[] namesList = names.split(", ");
        System.out.println(namesList[2]);

        // Scanner
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Palun sisesta oma nimi:");
//        String userName = scanner.nextLine();
//        System.out.println("Sinu nimi on " + userName);

        Scanner textProcessingScanner = new Scanner("Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.");
        textProcessingScanner.useDelimiter("Rida: ");
        while (textProcessingScanner.hasNext()) {
            System.out.println(textProcessingScanner.next());
        }
//        System.out.println(textProcessingScanner.next());
//        System.out.println(textProcessingScanner.next());
//        System.out.println(textProcessingScanner.next());
    }
}
