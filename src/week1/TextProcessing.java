package week1;

public class TextProcessing {
    public static void main(String[] args) {

        System.out.println("Original Elon Musk: " + args[0]);

        String elonMuskClone = "Elon Musk";
        System.out.println("Cloned Elon Musk: " + elonMuskClone);

        boolean areElonMusksEqual = args[0] == elonMuskClone; // Siin ma võrdlen kahte mäluaadressi, mis on erinevad.
        areElonMusksEqual = args[0].equals(elonMuskClone);
        System.out.println("Are Elon Musks equal? " + areElonMusksEqual);

    }
}
