package week1;

public class Day03If {
    public static void main(String[] args) {
        int num = Integer.parseInt(args[0]);

//        if (num % 2 == 0) {
//            System.out.println("Arv " + num + " on paaris");
//        } else {
//            System.out.println("Arv " + num + " on paaritu");
//        }

//        String outputText = (num % 2 == 0) ? String.format("Arv %d on paaris", num) : String.format("Arv %d on paaritu", num);
//        System.out.println(outputText);

        System.out.println((num % 2 == 0) ? String.format("Arv %d on paaris", num) : String.format("Arv %d on paaritu", num));

        // Switch demo

        switch(num) {
            case 2:
            case 3:
            case 4:
                System.out.println("Hinne oli keskpärane");
                break;
            case 5:
                System.out.println("Hinne oli täitsa hea");
                break;
            case 6:
                System.out.println("Hinne ületas ootusi");
                break;
            default:
                System.out.println("Hinne on tuvastamatu");
        }

    }
}
