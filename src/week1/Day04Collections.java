package week1;

import java.util.*;

public class Day04Collections {
    public static void main(String[] args) {
//        List<String> names = new ArrayList<>();
//        names.add("Ahti");
//        names.add("Ahto");
//        names.add("Leemet");
//        names.add("Linda");
//        System.out.println(names);
//
//        names.set(1, "Reet"); // meetod olemasoleva elemendi asendamiseks
//        System.out.println(names);
//
//        names.add(1, "Thomas"); // Lisab elemendi vahele
//        System.out.println(names);
//
//        // Elemendi eemaldamine listist
//        names.remove("Thomas");
//        names.remove(0);
//        System.out.println(names);
//
//        // Kas list sisaldab elementi?
//        System.out.println("Kas list sisaldab nime Linda? " + names.contains("Linda"));
//        System.out.println("Kas list sisaldab nime Alex? " + names.contains("Alex"));
//
//        // Mis indeksiga on listis etteantud element?
//        System.out.println("Mitmes element on Linda? " + names.indexOf("Linda"));
//        System.out.println("Mitmes element on Alex? " + names.indexOf("Alex"));
//
//        names.forEach(System.out::println);
//        names.forEach(t -> System.out.println("Töötaja nimi: " + t));
//
//        // Setis saab hoida ainult unikaalseid väärtuseid.
//        Set<String> namesSet = new HashSet<>();
//        namesSet.add("Rainer");
//        namesSet.add("Rainer");
//        namesSet.add("Rainer");
//        namesSet.add("Adalbert");
//        System.out.println(namesSet);
//
//        // Loome setist massiivi.
//        String[] namesArray = namesSet.toArray(String[]::new);
//
//        Iterator<String> namesSetIterator = namesSet.iterator();
//        int ind = 0;
//        while (namesSetIterator.hasNext()) {
//            if(ind == 56) {
//                System.out.println(namesSetIterator.next());
//            }
//            ind++;
//        }
//
//        int index = 0;
//        for(String name : namesSet) {
//            if (index == 1) {
//                System.out.println("Names set item: " + name);
//            }
//            index++;
//        }
//
        Map<String, String> estEngDict = new HashMap<>();
        estEngDict.put("auto", "car");
        estEngDict.put("isik", "person");
        estEngDict.put("riik", "country");
        estEngDict.put("sõiduk", "car");
//        System.out.println(estEngDict);
//        System.out.println(estEngDict.keySet());
//        System.out.println(estEngDict.values());
//        estEngDict.put("auto", "machine");
//        System.out.println(estEngDict);
//
//        estEngDict.replace("raamat", "book");
//        System.out.println(estEngDict);
//
//        estEngDict.remove("riik");
//        System.out.println(estEngDict);
//
//        for (String wordEst : estEngDict.keySet()) {
//            System.out.println("Sõna " + estEngDict.get(wordEst) + " on eesti keeles " + wordEst);
//        }

        for(Map.Entry<String, String> words : estEngDict.entrySet()) {
            System.out.println(words);
//            System.out.println("Sõna " + words.getValue() + " on eesti keeles " + words.getKey());
        }
    }
}
