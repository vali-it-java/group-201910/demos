package week1;

import java.util.*;

public class Day05Exercises {
    public static void main(String[] args) {

        // Exercise 1
        int rows = 6;
        int cols = 6;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                /*
                    rida 0: 6 * #
                    rida 1: 5 * #
                    rida 2: 4 * #

                    2 + 4 = 6

                    6 = 6 - 0
                    5 = 6 - 1
                    4 = 6 - 2
                    3 = 6 - 3
                 */
//                if (col + row < rows) { - VARIANT 1
                if (col >= row) { // VARIANT 2
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        // Exercise 2
        // VARIANT 1 (ilma if-ita)
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }

        // VARIANT 2 (if-iga)
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (col <= row) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        // Exercise 3
        System.out.println();
        rows = 5;
        cols = 5;
        for (int row = 0; row < rows; row++) {
            // rida 0 --> kolmnurga pikkus 4; rida 1 - kolmnurga pikkus 3, ...
            for (int col = 0; col < cols; col++) {
                if (col < 4 - row) {
                    System.out.print(" ");
                } else {
                    System.out.print("@");
                }
            }
            System.out.println();
        }

        // Exercise 4
        int number = 1234567;
        String numberStr = String.valueOf(number);
        String resultStr = "";
        for (char digit : numberStr.toCharArray()) {
            resultStr = digit + resultStr;
        }
        int result = Integer.parseInt(resultStr);
        System.out.println("Algne nunber: " + number + ", ümberpööratud number: " + result);

        StringBuilder sb = new StringBuilder(String.valueOf(389));
        System.out.println(sb.reverse());

        // Exercise 5
        for (int studentIndex = 0; studentIndex < args.length; studentIndex += 2) {

            int studentNameIndex = studentIndex;
            int studentScoreIndex = studentIndex + 1;

            String studentName = args[studentNameIndex];
            int studentScore = Integer.parseInt(args[studentScoreIndex]);
            int grade = 0;

            if (studentScore > 90) {
                grade = 5;
            } else if (studentScore > 80) {
                grade = 4;
            } else if (studentScore > 70) {
                grade = 7;
            } else if (studentScore > 60) {
                grade = 2;
            } else if (studentScore > 50) {
                grade = 1;
            }

            System.out.println("------");
            if (grade == 0) {
                System.out.printf("%s: FAIL\n", studentName);
            } else {
                System.out.printf("%s: PASS - %d, %d\n", studentName, grade, studentScore);
            }
        }

        // Exercise 6
        double[][] triangleSides = {
                {5.67, 9.98},
                {54.28, 41.44},
                {943.41, 19.1},
                {65, 99},
                {43.21, 11.11},
                {99.99, 100},
                {3, 4}
        };
        for (double[] triangle : triangleSides) {
            // Arvutage...
            double a = triangle[0];
            double b = triangle[1];
//                double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
//                double c = Math.sqrt(a*a + b*b);
            double c = Math.pow(a * a + b * b, 0.5);
            System.out.printf("Kolmnurgal külgedega %.2f ja %.2f on hüpotenuusi pikkus %.2f\n", a, b, c);
        }

        // Exercise 7
        String[][] countries = {
                {"Eesti", "Tallinn", "Jüri Ratas"},
                {"Soome", "Helsinki", "Antti Rinne"},
                {"Läti", "Riga", "Arturs Krišjānis Kariņš"},
                {"Leedu", "Vilnius", "Saulius Skvernelis"},
                {"Rootsi", "Stockholm", "Stefan Löfven"},
                {"Taani", "Copenhagen", "Mette Fredriksen"},
                {"Norra", "Oslo", "Erna Solberg"},
                {"Poola", "Warsaw", "Mateusz Morawiecki"},
                {"Venemaa", "Moscow", "Dmitry Medvedev"},
                {"Prantsusmaa", "Paris", "Édouard Philippe"}
        };

        System.out.println("Peaministrid:");
        for (String[] country : countries) {
            System.out.println("\t" + country[2]);
        }

        System.out.println("Riigid:");
        for (int i = 0; i < countries.length; i++) {
            System.out.printf("\tRiik: %s, pealinn: %s, peaminister: %s\n", countries[i][0], countries[i][1], countries[i][2]);
        }

        // Exercise 8

        // Variant 1
        String[][][] countries2 = {
                {{"Eesti"}, {"Tallinn"}, {"Jüri Ratas"}, {"Eesti", "Vene", "Soome"}},
                {{"Soome"}, {"Helsinki"}, {"Antti Rinne"}, {"Soome", "Rootsi", "Eesti"}},
                {{"Läti"}, {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Läti", "Vene"}},
                {{"Leedu"}, {"Vilnius"}, {"Saulius Skvernelis"}, {"Leedu", "Vene", "Läti", "Poola"}},
                {{"Rootsi"}, {"Stockholm"}, {"Stefan Löfven"}, {"Rootsi", "Saami", "Soome", "Taani", "Norra"}},
                {{"Taani"}, {"Copenhagen"}, {"Mette Fredriksen"}, {"Taani", "Rootsi"}},
                {{"Norra"}, {"Oslo"}, {"Erna Solberg"}, {"Norra"}},
                {{"Poola"}, {"Warsaw"}, {"Mateusz Morawiecki"}, {"Poola", "Saksa", "Leedu"}},
                {{"Venemaa"}, {"Moscow"}, {"Dmitry Medvedev"}, {"Vene", "Inglise", "Ukraina"}},
                {{"Prantsusmaa"}, {"Paris"}, {"Édouard Philippe"}, {"Prantsuse"}}
        };

        System.out.println("Riigid:");
        for (String[][] country : countries2) {
            System.out.println("\t" + country[0][0] + ", " + country[1][0] + ", " + country[2][0]);
            for (String countryLanguage : country[3]) {
                System.out.println("\t\t" + countryLanguage);
            }
        }

        System.out.println("Riigid veelkord:");
        for (int i = 0; i < countries2.length; i++) {
            System.out.println("\t" + countries2[i][0][0] + ", " + countries2[i][1][0] + ", " + countries2[i][2][0]);
            for (int j = 0; j < countries2[i][3].length; j++) {
                System.out.println("\t\t" + countries2[i][3][j]);
            }
        }

        // Variant 2
        Object[][] countries3 = {
                {"Eesti", "Tallinn", "Jüri Ratas", new String[]{"Eesti", "Vene", "Soome"}},
                {"Soome", "Helsinki", "Antti Rinne", new String[]{"Soome", "Rootsi", "Eesti"}}
        };

        System.out.println("Riigid kahetasandilises massiivis:");
        for (int i = 0; i < countries3.length; i++) {
            System.out.println("\t" + countries3[i][0] + ", " + countries3[i][1] + ", " + countries3[i][2]);
            String[] languages = (String[]) countries3[i][3];
            for (String language : languages) {
                System.out.println("\t\t" + language);
            }
        }

        // Exercise 9
        List<Map<String, String[]>> countries4 = new ArrayList<>();

        // Eesti
        Map<String, String[]> estonia = new HashMap<>();
        estonia.put("name", new String[]{"Eesti"});
        estonia.put("capital", new String[]{"Tallinn"});
        estonia.put("primeMinister", new String[]{"Jüri Ratas"});
        estonia.put("languages", new String[]{"Eesti", "Vene", "Soome", "Läti"});
        countries4.add(estonia);

        // Soome
        Map<String, String[]> finland = new HashMap<>();
        finland.put("name", new String[]{"Soome"});
        finland.put("capital", new String[]{"Helsinki"});
        finland.put("primeMinister", new String[]{"Antti Rinne"});
        finland.put("languages", new String[]{"Soome", "Rootsi", "Eesti"});
        countries4.add(finland);

        for (int i = 0; i < countries4.size(); i++) {
            System.out.println(String.format("%s, %s, %s",
                    countries4.get(i).get("name")[0], countries4.get(i).get("capital")[0], countries4.get(i).get("primeMinister")[0]));
            for (int j = 0; j < countries4.get(i).get("languages").length; j++) {
                System.out.println("\t" + countries4.get(i).get("languages")[j]);
            }
        }

        // Exercise 10
        Map<String, List<Object>> countries5 = new HashMap<>();

        // Eesti
        List<Object> estoniaItem = new ArrayList<>();
        estoniaItem.add("Tallinn");
        estoniaItem.add("Jüri Ratas");
        estoniaItem.add(new String[]{"Eesti", "Vene", "Soome", "Läti"});
        countries5.put("Eesti", estoniaItem);

        // Soome
        List<Object> finlandItem = Arrays.asList("Helsinki", "Antti Rinne", new String[]{"Soome", "Rootsi", "Eesti"});
        countries5.put("Soome", finlandItem);

        System.out.println("Harjutus 10");
        for (String countryName : countries5.keySet()) {
            System.out.println("\t" + countryName + ", " +
                    countries5.get(countryName).get(0) + ", " + countries5.get(countryName).get(1));
            String[] languages = (String[])countries5.get(countryName).get(2);
            for(String language : languages) {
                System.out.println("\t\t" + language);
            }
        }

        // Exercise 11
        Queue<List<Object>> countriesQueue = new LinkedList<>();
        countriesQueue.add(Arrays.asList("Eesti", "Tallinn", "Jüri Ratas", new String[]{"Eesti", "Vene", "Soome", "Ukraina"}));
        countriesQueue.add(Arrays.asList("Soome", "Helsinki", "Antti Rinne", new String[]{"Soome", "Rootsi", "Eesti"}));

        System.out.println("Riigid queuest:");
        while(!countriesQueue.isEmpty()) {
            List<Object> countryInfo = countriesQueue.remove();
            System.out.printf("%s, %s, %s:\n", countryInfo.get(0), countryInfo.get(1), countryInfo.get(2));
            String[] languages = (String[])countryInfo.get(3);
            for(int i = 0; i < languages.length; i++) {
                System.out.println("\t" + languages[i]);
            }
        }
    }

}
