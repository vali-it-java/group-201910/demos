

// See on testklass millegi lihtsa printimiseks standardväljundisse.
/* See on ka kommentaar */

// UpperCamelCase - klasside defineerimine
// ThisIsText
public class HelloWorld {

    // this_is_something - Pythoni stiil, snake case
    // WHAT_IS_THIS - Java konstandid, SCREAMING_SNAKE_CASE

    // lowerCamelCase - nii defineeritakse meetodi nimesid ja ka muutujaid
    public static void main(String[] args) {
        System.out.println("Tere, Maailm!");
        System.out.println("Hello world, " + args[4] + "!");
    }
}
