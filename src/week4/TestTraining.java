package week4;

import java.util.ArrayList;

public class TestTraining {
    public static void main(String[] args) {
        String result = analyzeNums(3, 4);
        System.out.println("Tulemus: " + result);

        Cat myCat = new Cat(45, 12, 123, 45);
        System.out.printf("Kassi omadused: saba pikkus: %d, küüne pikkus: %d, vurru pikkus: %d, kaal: %.2f\n",
                myCat.getTailLength(), myCat.getNailLength(), myCat.getWhiskerLength(), myCat.getWeight());


        String[] myNewStringArray = new String[]{"auto", "rebane", "sau"};
        String[] outArr = myStringArray(myNewStringArray);
        for (int i = 0; i < outArr.length; i++) {
            System.out.println(outArr[i]);
        }
    }

    public static String[] myStringArray(String[] stringArray) {
        String result = "";
        String patternedString = "";
        String[] myStringArray = new String[stringArray.length];
        for (int j = 0; j < stringArray.length; j++) {
            String myStrings = stringArray[j];
            char[] charArray = myStrings.toCharArray();
            for (int i = 0; i < charArray.length; i++) {
                result = result + "#";
            }
            myStringArray[j] = result;
            result = "";
            patternedString = "";
        }
        return myStringArray;
    }

    public static String temp(int temperature){
        String temp = "";
        if(temperature > 30){
            temp = "Leitsak";
        } else if (temperature >=25 && temperature <= 30){
            temp = "Rannailm";
        } else if (temperature >= 21 && temperature <= 24) {
            temp = "Mõnus suveilm";
        } else if (temperature >= 15 && temperature <= 20) {
            temp = "Jahe suveilm";
        } else if (temperature >=10 && temperature <= 14) {
            temp = "Jaani-ilm";
        } else if (temperature >=0 && temperature <= 9) {
            temp = "Telekavaatamise ilm";
        } else
            temp = "Suusailm";
        return temp;
    }

    public static String analyzeNums(int num1, int num2) {
        if (num1 == num2) {
            return "OK";
        } else {
            return "JAMA!";
        }
    }
}
