package week4;

public class Cat {
    private int tailLength;
    private int nailLength;
    private int whiskerLength;
    private double weight;

    public Cat(int tailLength, int nailLength, int whiskerLength, double weight) {
        this.tailLength = tailLength;
        this.nailLength = nailLength;
        this.whiskerLength = whiskerLength;
        this.weight = weight;
    }

    public int getTailLength() {
        return tailLength;
    }

    public void setTailLength(int tailLength) {
        this.tailLength = tailLength;
    }

    public int getNailLength() {
        return nailLength;
    }

    public void setNailLength(int nailLength) {
        this.nailLength = nailLength;
    }

    public int getWhiskerLength() {
        return whiskerLength;
    }

    public void setWhiskerLength(int whiskerLength) {
        this.whiskerLength = whiskerLength;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void makeNoise() {
        System.out.println("Bammm!");
    }

    public void jump() {
        System.out.println("Flying......");
    }
}
